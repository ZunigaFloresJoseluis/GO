![Go](https://www.juanjonavarro.com/images/31.png)

# GO

Go es un lenguaje concurrente y compilado(1) que ha sido desarrollado por 
Google por los desarrolladores Robert Griesemer,Rob Pike y Ken Thompson basandose en la sintaxis de C.
    
    "Go es un lenguaje de programación compilado, concurrente,
    imperativo, estructurado, orientado a objetos"

## Caracteristicas
* Go usa una sistanxis parecida a la de c
* Go admite el paradigma orientado a objetos pero no cuenta con palabras reservadas que denoten que lo es, para el poliformismo utiliza interfaces.
* Go usa un tipado statico como c.
* Go alternativamente usa el tipado dinamico.(Duck typing).
* Go tiene un recolector de basura como otros lenguajes esto lo hace muy mas rapido.
* Go no tiene Excepciones.
* En Go el ';' es opcional.
* Go no tiene Herencia.

## Lenguajes Compilados o Interpretados

Existen dos formas en las cuales un programa puede ejecutarse, las cuales son : **Interpretados,compilandose**.

Los lenguajes compilados son compiladores que generan un codigo maquina  basandose
en un codigo fuente.
Un compilador puede ser intermediario el cual ejecuta un doigo fuente muchas 
veces llamada ByteCode el cual es traducido a codigo maquina.


Los languajes Interpretados se diferencia de un lenguaje compilado es que mientras 
que los compiladores hacen una traduccion de todo el codigo el interpretado 
lo realiza  mientras se va pidiendo y  no va guardando ninguna traducción.

* * *
 - [1] Tareas ejecutandose simultaneamente, estas tareas pueden ser diferentes hilos 
    procesos.
 - [1] Los programas compilados son traducidos a codigo maquina para poder ser ejecutados son mas rapidos.

