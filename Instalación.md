# Instalación

Se puede encontrar toda la información de Go y su instalación en [GO](https://golang.org/)

![Linux](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b0/NewTux.svg/300px-NewTux.svg.png)

## Linux

Para instalar Go en linux se debe descargar los archivos necesarios que se encuentran en [go.tar](https://golang.org/doc/install?download=go1.11.linux-amd64.tar.gz)
cuando se halla descargado el archivo .tar 
    
Se ingresa a la carpeta de descargas de nuestro sistema descompriminos en la misma  con los comando 
       
        $ cd $HOME/Download/
        $ tar xvf go1.11.linux-amd64.tar.gz
    
Se accesa a la carpeta que se genero de nombre Go y se ingresa a la subcarpeta bin ahi 
se encuentran  el compilador y muchas mas herramientas para ejecutar programas con el lenguaje Go
    
Para que se pueda ejecutar desde cualquier  lugar de nuestra computadora debemos agregarlo variables del sistema
en linux esto se realiza agregando a nuestro archivo de shell que debe terminar en **.*rc** y puede ser bash, zsh o alguna otra y agregamos
        
        export PATH=$PATH:/usr/local/go/bin
        
    
    
    
    

![Windows](https://www.sysadminsdecuba.com/wp-content/uploads/2017/10/6165-windows-550x381.png)

## Windows